<?php

namespace app\blog\model;

use think\Db;
use think\Model;

class Article extends Model
{
    public function tags()
    {
        return $this->belongsToMany('Tags', 'ArticleTags', 'tags_id', 'article_id');
    }


}
