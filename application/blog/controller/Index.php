<?php
namespace app\blog\controller;

use think\Controller;
use think\Db;
use app\blog\model\Article;

class Index extends Controller
{
    protected $limit = 3; // 分页条数

    /**
     * 首页
     */
    public function index($page = 1, Article $model)
    {
        $count = $model->with('tags')->count();
        $article = $model->with('tags')->order('addtime','desc')->page($page, $this->limit)->select();

        $this->assign([
            'article' => $article,
            'count' => ceil($count / $this->limit),
            'page' => $page
        ]);
        return view();
    }

    /**
     * 文章页
     */
    public function article($id)
    {
        //阅读量加1
        Db::name('article')->where('id', $id)->setInc('view_num');

        $article = Db::name('article')->where('id', $id)->find();
        $this->assign('article', $article);
        return view();
    }

    /**
     * 标签页
     */
    public function tags($name, $page = 1, Article $model)
    {
        $count = $model
            ->alias('a')
            ->join('article_tags at', 'a.id=at.article_id')
            ->join('tags t', 'at.tags_id=t.id')
            ->where('t.tags_name', $name)
            ->with('tags')
            ->field('a.*')
            ->count();

        $article = $model
            ->alias('a')
            ->join('article_tags at', 'a.id=at.article_id')
            ->join('tags t', 'at.tags_id=t.id')
            ->where('t.tags_name', $name)
            ->with('tags')
            ->field('a.*')
            ->page($page, $this->limit)
            ->select();

        $this->assign([
            'article' => $article,
            'name' => $name,
            'count' => ceil($count / $this->limit),
            'page' => $page
        ]);

        return view();
    }

    /**
     * 友联
     */
    public function links()
    {
        return view();
    }

    /**
     * 关于我
     */
    public function about()
    {
        return view();
    }



}
