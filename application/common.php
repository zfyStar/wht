<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 调试打印函数
 */
function dd($value)
{
    dump($value);
    exit;
}

/**
 * 判断权限是否勾选
 */
function is_checked($rules, $id)
{
    if (in_array($id, $rules)) {
        echo "checked";
    }
}

/**
 * 时间戳格式化
 */
function datetime($time)
{
    return $time == 0 ? '无' : date('Y-d-m H:i', $time);
}

/**
 * 单位换算
 * @param  [int] $filesize [数据大小]
 * @return [string]   [带单位得字符串]
 */
function getFilesize($filesize)
{
    if ($filesize >= 1073741824) {
        $filesize = round($filesize / 1073741824 * 100) / 100 . ' gb';
    } elseif ($filesize >= 1048576) {
        $filesize = round($filesize / 1048576 * 100) / 100 . ' mb';
    } elseif ($filesize >= 1024) {
        $filesize = round($filesize / 1024 * 100) / 100 . ' kb';
    } else {
        $filesize = $filesize . ' bytes';
    }
    return $filesize;
}

/**
 * 根据设备获取链接条数
 */
function getLimit()
{
    //获取USER AGENT
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    //分析数据
    $is_pc = (strpos($agent, 'windows nt')) ? true : false;
    $is_iphone = (strpos($agent, 'iphone')) ? true : false;
    $is_ipad = (strpos($agent, 'ipad')) ? true : false;
    $is_android = (strpos($agent, 'android')) ? true : false;  
    //设置数据
    $limit = 56;
    if ($is_pc) {
        $limit = 56;
    }
    if ($is_iphone || $is_android) {
        $limit = 24;
    }
    if ($is_ipad) {
        $limit = 42;
    }
    return $limit;
}

/**
 * 获取网页title
 */
function getTitle($url)
{
    $f = file_get_contents($url);
    preg_match('/<title>(?<title>.*?)<\/title>/si', $f, $title); //获取title的正则表达式
    $encode = mb_detect_encoding($title['title'], array('GB2312', 'GBK', 'UTF-8', 'CP936')); //得到字符串编码
    $file_charset = iconv_get_encoding()['internal_encoding']; //当前文件编码
    if ($encode != 'CP936' && $encode != $file_charset) {
        return iconv($encode, $file_charset, $title['title']);
    }
    return $title['title'];
}

/** 
 * 多少时间前
 */
function mdate($time = null)
{
    $text = '';
    $time = strtotime($time);
    $time = $time === null || $time > time() ? time() : intval($time);
    $t = time() - $time; //时间差 （秒）
    $y = date('Y', $time) - date('Y', time());//是否跨年
    switch ($t) {
        case $t < 60:
            $text = 'Just'; // 一分钟内
            break;
        case $t < 60 * 60:
            $text = floor($t / 60) . 'minutes ago'; //一小时内
            break;
        case $t < 60 * 60 * 24:
            $text = floor($t / (60 * 60)) . 'hours ago'; // 一天内
            break;
        case $t < 60 * 60 * 24 * 3:
            // $text = floor($time / (60 * 60 * 24)) == 1 ? 'Yesterday ' . date('H:i', $time) : 'The day before yesterday' . date('H:i', $time); //昨天和前天
            $text = floor($time / (60 * 60 * 24)) == 1 ? 'Yesterday ' : 'The day before yesterday'; //昨天和前天
            break;
        case $t < 60 * 60 * 24 * 30:
            $text = floor($t / (60 * 60 * 30)) . 'days ago'; //一个月内
            break;
        case $t < 60 * 60 * 24 * 365 && $y == 0:
            $text = floor($t / (60 * 60 * 30 * 30)) . 'months ago'; //一年内
            break;
        default:
            $text = date('Y/m/d H:i:s', $time); //一年以前
            break;
    }
    return $text;
}

/**
 * 生成HTML摘要 nece001@163.com
 * @param string $html html内容
 * @param int $max 摘要长度
 * @param string $suffix 后缀
 * @return string
 */
function htmlSummary($html)
{
    preg_match_all('/<p>(.*?)<\/p>/', $html, $result);

    $text = '';
    foreach ($result[1] as $value) {
        $text .= $value;
    }
    $text = mb_substr(strip_tags($text), 0, 400);
    return $text;
}
