<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use app\index\model\UrlType;

class Index extends Controller
{
    /**
     * 首页
     */
    public function index(UrlType $model)
    {
        //关联查分类和链接
        $data = $model->with(['url' => function ($query) {
            $query->order('jump_num', 'desc');
        }])->order('sort', 'desc')->select();
    
        //常用
        $common = Db::name('url')->order('jump_num', 'desc')->limit(getLimit())->select();

        $this->assign([
            'data' => $data,
            'common' => $common
        ]);

        return view();
    }

    /**
     * 关于
     */
    public function about()
    {
        return view();
    }

    /**
     * 点击
     */
    public function jump()
    {
        $id = input('id');
        Db::name('url')->where('id', $id)->setInc('jump_num');
    }

    /** 
     * 添加网址
     */
    public function add()
    {
        $data = [
            'url' => input('url'),
            'name' => input('name'),
            'type' => input('type'),
            'addtime' => date('Y-m-d H:i:s', time())
        ];

        //验证
        if (input('pw') !== '776137') {
            $this->error('添加口令错误');
        }

        if (empty($data['url']) || empty($data['type']) || empty($data['type'])) {
            $this->error('请填写完整');
        }

        $res = Db::name('url')->insert($data);
        if ($res) {
            $this->success('添加成功');
        }
        $this->error('添加失败');
    }

    /**
     * 保存设置
     */
    public function setting()
    {
        $data = input('post.');
        $path = './../config/site.php';
        $file = include $path;
        $config = array_merge($file, $data);
        //生成文件字符串
        $str = "<?php " . PHP_EOL . "return [" . PHP_EOL;
        foreach ($config as $key => $value) {
            $str .= '   ' . '\'' . $key . '\'' . '=>' . '\'' . $value . '\'' . ',' . PHP_EOL;
        }
        $str .= '];';
        //写入文件
        if (file_put_contents($path, $str)) {
            $this->success('更新成功');
        } else {
            $this->error('更新失败');
        }
    }

    /** 
     * 获取标题
     */
    public function gettitle()
    {
        header("content-type:text/html; charset=utf-8");
        $url = input('url');
        $title = getTitle($url);
        if ($title) {
            $this->success($title);
        }
        $this->error('获取失败');
    }

}
