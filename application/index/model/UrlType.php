<?php

namespace app\index\model;

use think\Db;
use think\Model;

class UrlType extends Model
{

    /**
     * 关联链接模型
     */
    public function url()
    {
        return $this->hasMany('url', 'type');
    }

  
}
