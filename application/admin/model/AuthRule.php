<?php
namespace app\admin\model;

use think\Model;

class AuthRule extends Model
{

    /**
     * 权限树
     */
    public function authRuleTree()
    {
        $authRuleres = $this->order('sort desc')->select();
        return $this->sort($authRuleres);
    }

    /**
     * 生成树结构
     */
    public function sort($data, $pid = 0, $level = 0)
    {
        static $arr = array();
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level;
                $arr[] = $v;
                $this->sort($data, $v['id'], $level + 1);
            }
        }
        return $arr;
    }

    /**
     * 获取菜单
     */
    public function getMenu($rule)
    {
        $where = [];
        $where[] = ['is_show', '=', 1];
        if (!empty($rule)) {
            $where[] = ['id', 'in', $rule];
        }
        $authRuleres = $this->order('sort desc')->where($where)->select();
        return $this->sorts($authRuleres);
    }

    public function sorts($data, $pid = 0, $level = 0)
    {
        $arr = array();
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level;
                $v['son'] = $this->sorts($data, $v['id'], $level + 1);
                $arr[] = $v;
            }
        }
        return $arr;
    }



}
