<?php
namespace app\admin\model;

use think\Model;

class GoodsCategory extends Model
{
    /**
     * 分类树
     */
    public function getTree()
    {
        $CategoryTree = $this->order('sort desc')->select();
        return $this->sort($CategoryTree);
    }

    /** 
     * 生成树结构
     */
    public function sort($data, $pid = 0, $level = 0)
    {
        static $arr = array();
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level;
                $arr[] = $v;
                $this->sort($data, $v['id'], $level + 1);
            }
        }
        return $arr;
    }





}
