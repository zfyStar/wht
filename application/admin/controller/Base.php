<?php
namespace app\admin\controller;

use think\Controller;
use com\Auth;
use think\Db;

class Base extends Controller
{
    public function initialize()
    {
        //登陆验证
        if (!session('id')) {
            $this->redirect('login/login');
        }

        define('USER_ID', session('id'));

        //超级管理员组不检测
        if (session('group_id') != 1) {

            $con = $this->request->controller();
            $action = $this->request->action();
            $rule = $con . '/' . $action;
            $noCheck = ['Index/index', 'Index/main', 'Admin/norule'];

            //可以跳过验证的
            if (!in_array($rule, $noCheck)) {
                //权限验证
                $auth = new Auth();
                if (!$auth->check($rule, USER_ID)) {
                    //跳转无权限页
                    $this->error('没有权限', url('Admin/norule'), '', 1);
                }
            }
        }
    }

    /**
     * 更改状态
     */
    public function up_state()
    {
        $state = input('value') == 1 ? 0 : 1;
        $id = input('id');
        $name = input('name');

        $res = Db::name(input('date'))->where('id', $id)->update([$name => $state]);
        if ($res) {
            $this->success('修改成功');
        }
        $this->error('修改失败');
    }

    /**
     * 入库操作
     * @param  [string] $db   [数据库名]
     * @param  [array] $data [要入库的数据]
     */
    public function toDb($db, $data)
    {
        if (input('id')) {
            $res = Db::name($db)->where('id', input('id'))->update($data);
            if ($res) {
                $this->success('修改成功');
            }
            $this->error('修改失败');
        } else {
            $res = Db::name($db)->insert($data);
            if ($res) {
                $this->success('添加成功');
            }
            $this->error('添加失败');
        }
    }

    /**
     * 删除操作
     */
    public function data_del()
    {
        if ($this->request->isPost()) {
            $res = Db::name(input('db'))->where('id', input('id'))->delete();
            if ($res) {
                $this->success('删除成功');
            }
            $this->error('删除失败');
        }
    }

    /**
     * 批量删除
     */
    public function all_del()
    {
        if ($this->request->isPost()) {
            $id = substr(input('id'), 1);
            $ids = explode(',', $id);

            $res = Db::name(input('db'))->delete($ids);
            if ($res) {
                $this->success('删除成功');
            }
            $this->error('删除失败');
        }
    }


}
