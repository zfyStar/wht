<?php
namespace app\admin\controller;

use think\Db;

class Goods extends Base
{

    /**
     * 商品分类列表
     */
    public function goods_category()
    {
        $data = model('GoodsCategory')->getTree();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 商品分类添加修改
     */
    public function category_add()
    {
        if ($this->request->isPost()) {
            $data = [
                'name' => input('name'),
                'pid' => input('pid'),
                'sort' => input('sort'),
                'thumb' => input('thumb'),
                'status' => input('status')
            ];

            //入库
            $db = 'goods_category';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('goods_category')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }

        //分类
        $type = model('GoodsCategory')->getTree();
        $this->assign('type', $type);
        return view();
    }

    /**
     * 商品列表
     */
    public function goods()
    {
        $data = Db::name('goods')->alias('g')->join('__GOODS_CATEGORY__ c', 'g.goods_category_id=c.id')
            ->fieldRaw('g.id,g.name,c.name as category,g.thumb,g.price,g.createtime,g.status')
            ->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 增加商品
     */
    public function goods_add()
    {
        if ($this->request->isPost()) {

            $data = input('post.');
            $data['price'] = $data['price'] * 100;
            $data['thumblist'] = serialize($_POST['thumblist']);
            if (input('id')) {
                $data['updatetime'] = time();
            } else {
                $data['createtime'] = time();
            }

            //入库
            $db = 'goods';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('goods')->where('id', input('id'))->find();
            $data['thumblist'] = unserialize($data['thumblist']);
            $this->assign('data', $data);
        }

        $type = model('GoodsCategory')->getTree();
        $this->assign('type', $type);
        return view();
    }




}
