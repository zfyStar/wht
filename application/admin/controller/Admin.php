<?php
namespace app\admin\controller;

use com\Auth;
use think\Db;

class Admin extends Base
{
    /**
     * 管理员列表
     */
    public function user()
    {
        $data = Db::name('admin_user')->paginate();
        $auth = new Auth();
        foreach ($data as $key => $value) {
            $res = $auth->getGroups($value['id']);
            $value['title'] = $res[0]['title'];
            $datas[] = $value;
        }
        $this->assign('data', $datas);
        return view();
    }

    /**
     * 管理员添加
     */
    public function user_add()
    {
        if ($this->request->isPost()) {
            $data = [
                'name' => input('name'),
                'password' => md5(input('password')),
                'head_img' => input('head_img'),
                'is_lock' => input('is_lock'),
                'add_time' => time()
            ];

            //开始事物操作
            Db::startTrans();

            $userId = Db::name('admin_user')->insertGetId($data);
            $res = Db::name('auth_group_access')->insert([
                'uid' => $userId,
                'group_id' => input('group')
            ]);

            if ($userId && $res) {
                // 提交事务
                Db::commit();
                $this->success('添加成功');
            } else {
                Db::rollback();
                $this->error('添加失败');
            }
        }

        if ($this->request->isGet()) {
            $group = Db::name('auth_group')->select();
            $this->assign('group', $group);
            return view();
        }
    }

    /**
     * 管理员修改
     */
    public function user_edit()
    {
        if ($this->request->isPost()) {

            $input = input('post.');
            $password = Db::name('admin_user')->where('id', $input['id'])->value('password');
            $password = input('password') == $password ? $password : md5($input['password']);

            $data = [
                'name' => $input['name'],
                'password' => $password,
                'head_img' => $input['head_img'],
                'is_lock' => $input['is_lock'],
                'add_time' => time()
            ];
        
            //开始事物操作
            Db::startTrans();

            $user = Db::name('admin_user')
                ->where('id', $input['id'])
                ->update($data);

            $res = Db::name('auth_group_access')
                ->where('uid', input('id'))
                ->update(['group_id' => $input['group']]);

            if ($user) {
                // 提交事务
                Db::commit();
                $this->success('修改成功');
            } else {
                Db::rollback();
                $this->error('修改失败');
            }
        }

        if ($this->request->isGet()) {

            $data = Db::name('admin_user au')
                ->join('auth_group_access aa', 'au.id=aa.uid', 'left')
                ->field('au.*,aa.group_id')
                ->where('au.id', input('id'))
                ->find();
            $this->assign('data', $data);

            $group = Db::name('auth_group')->select();
            $this->assign('group', $group);
            return view();
        }
    }

    /**
     * 管理员删除
     */
    public function user_del()
    {
        if ($this->request->isPost()) {
            $id = input('id');
            //开始事物操作
            Db::startTrans();

            $res = Db::name('admin_user')->where('id', $id)->delete();
            $users = Db::name('auth_group_access')->where('uid', $id)->delete();

            if ($users && $res) {
                // 提交事务
                Db::commit();
                $this->success('删除成功');
            } else {
                Db::rollback();
                $this->error('删除失败');
            }
        }
    }

    /**
     * 分组列表
     */
    public function group()
    {
        $data = Db::name('auth_group')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 分组添加修改
     */
    public function group_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'title' => input('title'),
                'status' => input('status'),
                'add_time' => time()
            ];

            //入库
            $db = 'auth_group';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('auth_group')->where('id', input('id'))->find();
            $this->assign('data', $data);

            return view();
        }
    }

    /**
     * 分组选权限
     */
    public function group_rules()
    {
        //选择权限
        if ($this->request->isPost()) {
            $rules = substr(input('rules'), 1);
            $res = Db::name('auth_group')->where('id', input('id'))->update(['rules' => $rules]);
            if ($res) {
                $this->success('修改成功');
            }
            $this->error('修改失败');
        }

        if ($this->request->isGet()) {
            $id = input('id');
            $this->assign('id', $id);
        
            //把他拥有的权限分割成数组分配下去判断
            $rules = Db::name('auth_group')->where('id', input('id'))->find();
            $rules = explode(',', $rules['rules']);
            $this->assign('rules', $rules);
            //权限树
            $authRule = model('authRule');
            $data = $authRule->authRuleTree();
            $this->assign('data', $data);

            return view();
        }
    }

    /**
     * 管理组删除
     */
    public function group_del()
    {
        if ($this->request->isPost()) {
            //开始事物操作
            Db::startTrans();

            $res = Db::name('auth_group')->where('id', input('id'))->delete();
            $gres = Db::name('auth_group_access')->where('group_id', input('id'))->delete();

            if ($res) {
                // 提交事务
                Db::commit();
                $this->success('删除成功');
            } else {
                Db::rollback();
                $this->error('删除失败');
            }
        }
    }

    /**
     * 权限列表
     */
    public function rules()
    {
        $authRule = model('authRule');
        $data = $authRule->authRuleTree();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 权限增加修改
     */
    public function rules_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'pid' => input('pid'),
                'name' => input('name'),
                'title' => input('title'),
                'status' => input('status'),
                'icon' => input('icon'),
                'sort' => input('sort'),
                'is_show' => input('is_show')
            ];

            //入库
            $db = 'auth_rule';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('auth_rule')->where('id', input('id'))->find();
            $this->assign('data', $data);

            $rules = Db::name('auth_rule')->field('id,title')->where('pid', 0)->select();
            $this->assign('rules', $rules);
            return view();
        }
    }

    /**
     * 选择图标
     */
    public function choice_icon()
    {
        return view();
    }

    /**
     * 无权限跳转页
     */
    public function norule()
    {
        return view();
    }


}
