<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Wechat extends Base
{
    /**
     * 菜单列表 
     */
    public function menu()
    {
        $data = model('WechatMenu')->getTree();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 菜单添加
     */
    public function menu_add()
    {
        if ($this->request->isPost()) {
            $data = [
                'name' => input('name'),
                'pid' => input('pid'),
                'sort' => input('sort'),
                'url' => input('url'),
                'status' => input('status'),
                'type' => input('type')
            ];

            //入库
            $db = 'wechat_menu';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('wechat_menu')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }

        //分类
        $type = model('WechatMenu')->getTree();
        $this->assign('type', $type);
        return view();
    }

    /**
     * 生成菜单
     */
    public function setMenu()
    {
        if ($this->request->isAjax()) {
            $data['button'] = model('WechatMenu')->getMenu();
            $menu = load_wechat('menu');
            $result = $menu->createMenu($data);

            // 处理创建结果
            if ($result === false) {
                $this->error($menu->errMsg);
            }
            $this->success('设置菜单成功');
        }
    }

    /**
     * 自动回复
     */
    public function reply()
    {
        $data = Db::name('wechat_reply')->order('createtime desc')->paginate();
        foreach ($data as $key => $value) {
            if ($value['type'] == 'news') {
                $value['content'] = unserialize($value['content'])['Title'];
            }
            $arr[] = $value;
        }
        $this->assign('data', $arr);
        return view();
    }

    /**
     * 回复添加
     */
    public function reply_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'keyword' => input('keyword'),
                'type' => input('type'),
                'content' => input('content'),
                'status' => input('status'),
                'createtime' => time()
            ];

            //图文
            if ($data['type'] == 'news') {
                $data['content'] = serialize($_POST['news']);
            }

            //入库
            $db = 'wechat_reply';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('wechat_reply')->where('id', input('id'))->find();
            if ($data['type'] == 'news') {
                $data['content'] = unserialize($data['content']);
            }
            $this->assign('data', $data);
        }

        return view();
    }

    /**
     * 粉丝列表
     */
    public function fans()
    {
        $data = Db::name('wechat_user')->order('createtime desc')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 更新粉丝信息
     */
    public function update_fans()
    {
        if ($this->request->isGet()) {
            $openid = input('openid');

            $user = load_wechat('User');
            $info = $user->getUserInfo($openid);

            if ($result === false) {
                $this->error($user->errMsg);
            }

            $data = [
                'openid' => $info['openid'],
                'headimgurl' => $info['headimgurl'],
                'nickname' => $info['nickname'],
                'city' => $info['city'],
                'sex' => $info['sex'],
                'createtime' => $info['subscribe_time']
            ];

            $res = Db::name('wechat_user')->where(['openid' => $info['openid']])->update($data);
            if ($res) {
                $this->success('更新成功');
            }
            $this->error('暂无更新');
        }
    }

    /**
     * 拉取粉丝
     */
    public function getFans()
    {
        if ($this->request->isAjax()) {
            $user = load_wechat('User');

            $result = $user->getUserList();

            if ($result === false) {
                $this->error($user->errMsg);
            }

            $UserInfo = $result['data']['openid'];

            foreach ($UserInfo as $key => &$value) {
                $value = $user->getUserInfo($value);
            }

            return json($UserInfo);
        }
    }

    /**
     * 素材管理
     */
    public function media()
    {
        $data = Db::name('wechat_media')->order('createtime desc')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 素材添加
     */
    public function media_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'name' => input('name'),
                'type' => input('type'),
                'content' => input('mediapath'),
                'createtime' => time()
            ];

            $media = load_wechat('Media');

            //图文
            if ($data['type'] == 'news') {
                $news = $_POST['news'];
                //得到封面media_id
                $result = $this->wxsc($data['content'], 'image');
                $news['thumb_media_id'] = $result['media_id'];

                $articles = array();
                $articles['articles'][] = $news;
                $result = $media->uploadForeverArticles($articles);
                //返回结果
                if ($result === false) {
                    $this->error('失败' . $media->errMsg);
                } else {
                    $data['content'] = serialize($news);
                }
            } else {
                $result = $this->wxsc($data['content'], $data['type']);
            }

            $data['media_id'] = $result['media_id'];

            //入库
            $db = 'wechat_media';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('wechat_media')->where('id', input('id'))->find();
            $data['content'] = unserialize($data['content']);
            $this->assign('data', $data);
        }

        return view();
    }

    /**
     * 图文素材修改
     */
    public function media_edit()
    {
        //表单处理
        if ($this->request->isPost()) {

            $data = [
                'name' => input('name'),
                'createtime' => time()
            ];

            $news = $_POST['news'];
            //上传了封面重新获取media_id
            if (!empty(input('mediapath'))) {
                $result = $this->wxsc($data['content'], 'image');
                $news['thumb_media_id'] = $result['media_id'];
            }

            $articles = array();
            $articles['articles'] = $news;

            $media = load_wechat('Media');
            $media_id = input('media_id');
            $result = $media->updateForeverArticles($media_id, $articles, 0);
 
            //返回结果
            if ($result === false) {
                $this->error('失败' . $media->errMsg);
            }
            $data['content'] = serialize($news);
            
            //入库
            $db = 'wechat_media';
            $this->toDb($db, $data);
        }

        //展示页面
        if ($this->request->isGet()) {
            $data = Db::name('wechat_media')->where('id', input('id'))->find();
            $data['content'] = unserialize($data['content']);
            $this->assign('data', $data);
        }

        return view();
    }

    /**
     * 永久图片、语音、图文封面图
     * @param  [srting] $path [路径]
     * @param  [srting] $type [类型]
     * @return [array]       [结果]
     */
    public function wxsc($path, $type)
    {
        $media = load_wechat('Media');

        if (class_exists('\CURLFile')) {
            $fileData = array('media' => new \CURLFile(realpath('.' . $path)));
        } else {
            $fileData = array('media' => '@' . realpath('.' . $path));
        }
        $result = $media->uploadForeverMedia($fileData, $type);
        if ($result === false) {
            $this->error('失败' . $media->errMsg);
        }
        return $result;
    }

    /**
     * 上传图片到微信服务器（不占用素材资源数据）
     */
    public function contentimg()
    {
        $file = $this->request->file('file');
        if (!$file) {
            return json(["code" => 1, "msg" => '上传错误']);
        }

        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if (!$info) {
            return json(["code" => 1, "msg" => $file->getError()]);
        }

        $path = './uploads/' . $info->getSaveName();
        if (class_exists('\CURLFile')) {
            $fileData = array('media' => new \CURLFile(realpath($path)));
        } else {
            $fileData = array('media' => '@' . realpath($path));
        }

        $media = load_wechat('Media');
        $result = $media->uploadImg($fileData);
        // 处理执行的结果
        if ($result === false) {
            $data = ["code" => 1, "msg" => $media->errMsg];
        } else {
            $data = ["code" => 0, "msg" => '上传成功', "data" => ["src" => $result['url']]];
        }

        return json($data);
    }

    /**
     * 删除素材
     */
    public function media_del()
    {
        if ($this->request->isPost()) {
            $data = Db::name('wechat_media')->where('id', input('id'))->find();
            $media = load_wechat('Media');
            // 删微信
            $result = $media->delForeverMedia($data['media_id']);
            if ($result === false) {
                $this->error('失败' . $media->errMsg);
            }
            //成功后删本地
            $res = Db::name('wechat_media')->where('id', input('id'))->delete();
            if ($res) {
                $this->success('删除成功');
            }
            $this->error('删除失败');
        }
    }



}
