<?php
namespace app\admin\controller;

use com\Qiniu;
use think\Db;

class Upload extends Base
{

    /**
     * 上传图片
     */
    public function uploadimg()
    {
        $file = $this->request->file('file');
        if (!$file) {
            return json(["code" => 0, "msg" => '上传错误']);
        }

        $config = config('qiniu.');
        //判断七牛云存储是否开启
        if ($config['status'] == '1') {
            $file = $file->getInfo();
            $qiniu = new Qiniu($config);
            $save = array('saveName' => 'zfy/img/' . date('Ymd') . '/' . uniqid() . '.png');
            $info = $qiniu->upload(array('fileName' => $file['name'], 'temp' => $file["tmp_name"]), $save, 'file');
            if (is_array($info) && !isset($info['error'])) {
                $src = $config['divdomain'] . '/' . $info['key'];
                $data = ["code" => 1, "msg" => '上传成功', "src" => $src];
            } else {
                $data = ["code" => 0, "msg" => "上传失败"];
            }
        } else {
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if ($info) {
                $path = '/uploads/' . $info->getSaveName();
                $data = ["code" => 1, "msg" => '上传成功', "src" => $path];
            } else {
                $data = ["code" => 0, "msg" => $file->getError()];
            }
        }
        return json($data);
    }

    /**
     * layui编辑器图片
     */
    public function contentimg()
    {
        $file = $this->request->file('file');
        if (!$file) {
            return json(["code" => 1, "msg" => '上传错误']);
        }

        $config = config('qiniu.');
        //判断七牛云存储是否开启
        if ($config['status'] == '1') {
            $file = $file->getInfo();
            $qiniu = new Qiniu($config);
            $save = array('saveName' => 'zfy/layui/' . date('Ymd') . '/' . uniqid() . '.png');
            $info = $qiniu->upload(array('fileName' => $file['name'], 'temp' => $file["tmp_name"]), $save, 'file');
            if (is_array($info) && !isset($info['error'])) {
                $src = $config['divdomain'] . '/' . $info['key'];
                $data = ["code" => 0, "msg" => '上传成功', "data" => ["src" => $src]];
            } else {
                $data = ["code" => 1, "msg" => "上传失败"];
            }
        } else {
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if ($info) {
                $path = '/uploads/' . $info->getSaveName();
                $data = ["code" => 0, "msg" => '上传成功', "data" => ["src" => $path]];
            } else {
                $data = ["code" => 1, "msg" => $file->getError()];
            }
        }
        return json($data);
    }

    /**
     * wangEditor编辑器图片
     */
    public function wangEditor()
    {
        $file = $this->request->file('FileName');
        $config = config('qiniu.');
        //判断七牛云存储是否开启
        if ($config['status'] == '1') {
            $file = $file->getInfo();
            $qiniu = new Qiniu($config);
            $save = array('saveName' => 'zfy/editor/' . date('Ymd') . '/' . uniqid() . '.png');
            $info = $qiniu->upload(array('fileName' => $file['name'], 'temp' => $file["tmp_name"]), $save, 'file');
            if (is_array($info) && !isset($info['error'])) {
                echo $config['divdomain'] . '/' . $info['key'];
            }
        } else {
            $info = $file->validate(['ext' => 'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
            if ($info) {
                $path = '/uploads/' . $info->getSaveName();
                echo $path;
            }
        }
    }

    /**
     * 上传文件
     */
    public function upload()
    {
        $file = $this->request->file('file');

        if (!$file) {
            return json(["code" => 0, "msg" => '上传错误']);
        }

        $config = config('qiniu.'); //获取七牛云配置

        //判断七牛云存储是否开启
        if ($config['status'] == '1') {
            $file = $file->getInfo();
            $qiniu = new Qiniu($config);

            $save = array('saveName' => 'zfy/file/' . date('Ymd') . '/' . uniqid() . '.png');
            $info = $qiniu->upload(array('fileName' => $file['name'], 'temp' => $file["tmp_name"]), $save, 'file');
            //上传成功
            if (is_array($info) && !isset($info['error'])) {
                $src = $config['divdomain'] . '/' . $info['key'];
                $data = ["code" => 0, "msg" => '上传成功', "data" => ["src" => $src]];
            }
            //失败
            $data = ["code" => 1, "msg" => "上传失败"];
        } else {
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if ($info) {
                $path = '/uploads/' . $info->getSaveName();
                $data = ["code" => 1, "msg" => '上传成功', "src" => $path];
            } else {
                $data = ["code" => 0, "msg" => $file->getError()];
            }
        }

        return json($data);
    }


}
