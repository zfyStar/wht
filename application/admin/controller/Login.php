<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Login extends Controller
{
    /**
     * 登陆页面
     */
    public function login()
    {
        if ($this->request->isPost()) {
            $this->check(input('code'));

            $where = array('name' => input('username'), 'password' => md5(input('password')));
            $res = Db::name('admin_user')
                ->where($where)->alias('u')
                ->join('__AUTH_GROUP_ACCESS__ a', 'u.id=a.uid')
                ->fieldRaw('u.*,a.group_id')->find();
            if (!$res) {
                $this->error('用户名或密码错误！');
            }

            session('id', $res['id']);
            session('group_id', $res['group_id']);
            $this->success('登陆成功', url('admin/index/index'));
        }
        return $this->fetch();
    }

    /**
     * 验证码检测
     */
    public function check($code = '')
    {
        if (!captcha_check($code)) {
            $this->error('验证码错误');
        }
        return true;
    }

    /**
     * 修改密码
     */
    public function password()
    {
        if ($this->request->isPost()) {
           $post = input('post.');

            $where = array('id' => session('id'), 'password' => md5($post['oldPwd']));
            if (!Db::name('admin_user')->where($where)->find()) {
                $this->error('旧密码错误');
            }
            if (!empty($post['newPwd']) && $post['newPwd'] == $post['confirmPwd']) {
                $res = Db::name('admin_user')->where('id', session('id'))->strict(false)->update(['password'=>md5($post['newPwd'])]);
                $this->success('修改密码成功');
            }
            $this->error('新密码不一致或为空');
        }

        if ($this->request->isGet()) {
            $user = Db::name('admin_user')->where('id',session('id'))->find();
            $this->assign('user',$user);
            return $this->fetch();
        }
    }

    /**
     * 退出登陆
     */
    public function loginout()
    {
        session(null);
        $this->success('退出成功', url('admin/login/login'), '', 1);
    }






}
