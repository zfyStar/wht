<?php
namespace app\admin\controller;

use \tp5er\Backup;
use think\Db;

class Backups extends Base
{
    public $backup;

    /**
     * 初始化函数
     */
    public function initialize()
    {
        parent::initialize();
        $this->backup = new Backup(config('backup.'));
    }

    /**
     * 数据库列表
     */
    public function backup()
    {
        $data = $this->backup->dataList();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 备份表
     */
    public function backupTable()
    {
        $name = substr(input('name'), 1);
        $names = explode(',', $name);

        foreach ($names as $value) {
            $file = ['name' => date('Ymd-His'), 'part' => 1];
            $res = $this->backup->setTimeout(0)->setFile($file)->backup($value, 0);
        }

        if ($res == 0) {
            $this->success('备份成功');
        }

        $this->error('备份文件删除失败，请检查权限！');

    }

    /**
     * 备份列表
     */
    public function data_list()
    {
        $data = $this->backup->fileList();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 删除备份文件
     */
    public function delete()
    {
        $time = input('time');
        $res = $this->backup->delFile($time);

        if ($res == $time) {
            $this->success('删除成功');
        }

        $this->error('删除失败');

    }

    /**
     * 下载备份文件
     */
    public function downloadFile()
    {
        $time = input('time');
        $this->backup->downloadFile($time);
    }

    /**
     * 导入还原
     */
    public function importFile()
    {
        $time = input('time');

        $file = $this->backup->getFile('timeverif', $time);
        $res = $this->backup->setTimeout(0)->setFile($file)->import(0);
        if ($res == 0) {
            $this->success('还原成功');
        }
        $this->error('还原失败');
    }

    /**
     * 优化表
     */
    public function optimize()
    {
        $post = input('post.');

        if ($post['num'] == 1) {
            $res = $this->backup->optimize($post['name']);  //成功返回表名，失败返回错误信息
            if ($res != $post['name']) {
                $this->error($res);
            }
        } else {
            $name = substr($post['name'], 1);
            $names = explode(',', $name);

            foreach ($names as $value) {
                $res = $this->backup->optimize($value);
                if ($res != $value) {
                    $this->error($res);
                }
            }
        }

        $this->success('优化成功');
    }

    /**
     * 修复表
     */
    public function repair()
    {
        $post = input('post.');

        if ($post['num'] == 1) {
            $res = $this->backup->repair($post['name']);  //返回数组
            if ($res[0]['Msg_text'] != 'OK') {
                $this->error($res[0]['Msg_text']);
            }
        } else {
            $name = substr($post['name'], 1);
            $names = explode(',', $name);

            foreach ($names as $value) {
                $res = $this->backup->repair($value);
                if ($res[0]['Msg_text'] != 'OK') {
                    $this->error($res[0]['Msg_text']);
                }
            }
        }

        $this->success('修复成功');
    }





}   