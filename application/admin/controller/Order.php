<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Order extends Base
{

    /**
     * 订单列表
     */
    public function order()
    {
        $where = $this->getWhere();
        
        //搜索重新回第一页
        if ($this->request->isPost()) {
            $this->request->get(['page' => 1]);
        }
        $data = Db::name('order')->where($where)->order('createtime desc')
            ->paginate(2, '', ['query' => $this->request->param()]);
        $this->assign('data', $data);
        return view();
    }


    /**
     * 订单导出
     */
    public function order_export()
    {
        $where = $this->getWhere();

        $header = array('订单ID', '用户ID', '订单号', '订单状态', '支付状态', '商品金额', '运费', '付款时间', '创建时间');
        $data = Db::name('order')->where($where)->field('user_address_id', true)->order('createtime desc')->select();

        foreach ($data as $k => $v) {
            $data[$k]['orderstatus'] = orderstatus($data[$k]['orderstatus']);
            $data[$k]['paystatus'] = paystatus($data[$k]['paystatus']);
            $data[$k]['createtime'] = datetime($data[$k]['createtime']);
            $data[$k]['paytime'] = datetime($data[$k]['paytime']);
        }

        $objPHPExcel = new \PHPExcel();
        $objSheet = $objPHPExcel->getActiveSheet();
        $objSheet->setTitle("订单数据表");
        //设置表头
        $key = ord("A");
        foreach ($header as $v) {
            $colum = chr($key);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colum)->setWidth(15);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colum . '1', $v);
            $key += 1;
        }
        $column = 2;
        $objSheet = $objPHPExcel->getActiveSheet();
        $objSheet->getRowDimension(1)->setRowHeight(20);
        foreach ($data as $key => $rows) { //行写入
            $span = ord("A");
            foreach ($rows as $keyName => $value) {// 列写入
                $j = chr($span);
                $objSheet->getRowDimension($column)->setRowHeight(20);
                $objSheet->setCellValue($j . $column, $value);
                $span++;
            }
            $column++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');//生成一个Excel2007文件
        $path = 'execl/order/' . date('YmdHis', time()) . '.xlsx';
        $objWriter->save($path);

        $res = $objWriter ? ['code' => 1, 'msg' => $path] : ['code' => 0, 'msg' => '操作错误'];

        return json($res);
    }

    /**
     * 搜索条件
     */
    public function getWhere()
    {
        $where = [];
    
        //时间
        if (input('begin') && input('end')) {
            $begin = strtotime(input('begin'));
            $end = strtotime(input('end'));
            $where['createtime'] = [['>', $begin], ['<', $end]];
        }
        //状态
        if (input('paystatus') != 'all' && input('paystatus') != null) {
            $where['paystatus'] = input('paystatus');
        }
        //关键词
        if (input('keywords')) {
            if (input('name') == 'order') {
                $where['ordersn'] = input('keywords');
            } elseif (input('name') == 'user') {
                $where['user_id'] = input('keywords');
            }
        }
        return $where;
    }

    /**
     * 订单修改
     */
    public function order_edit()
    {
        if ($this->request->isPost()) {

            $data = [
                'paystatus' => input('paystatus'),
                'orderstatus' => input('orderstatus'),
                'shippingfee' => input('shippingfee')
            ];

            if (input('id')) {
                $res = Db::name('order')->where('id', input('id'))->update($data);
                if ($res) {
                    $this->success('修改成功');
                    die;
                } else {
                    $this->error('修改失败');
                    die;
                }
            }

        }

        if ($this->request->isGet()) {
            $data = Db::name('order')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }
        return view();
    }

    /**
     * 订单详情
     */
    public function order_details()
    {
        $data = Db::name('order_goods')->where('order_id', input('id'))->select();
        $this->assign('data', $data);

        $addr = $data['user_address_id'];
        return view();
    }






}
