<?php
namespace app\admin\controller;

use think\Db;

class Index extends Base
{
    /**
     * 首页
     */
    public function index()
    {   
        //用户资料
        $user = Db::name('admin_user')->where('id', USER_ID)->find();
        $this->assign('user', $user);
        //不同角色显示不同菜单
        if (session('group_id') == 1) {
            $rule = null;
        } else {
            $rule = Db::name('auth_group')->where('id', session('group_id'))->find();
        }
        //获取菜单
        $menu = model('authRule')->getMenu($rule['rules']);
        $this->assign('menu', $menu);
        //搜索框数据
        $where = [];
        $where[] = ['is_show', '=', 1];
        $where[] = ['status', '=', 1];
        if ($rule) {
            $where[] = ['id', 'in', $rule['rules']];
        }
        $search = Db::name('authRule')
            ->where($where)
            ->field('name,title,id,pid')
            ->orderRaw("rand()")
            ->select();
        $this->assign('searchMenu', $search);

        return view();
    }

    /**
     * 解锁
     */
    public function unlock()
    {
        $password = input('passwd');
        $res = Db::name('admin_user')
            ->where(['id' => USER_ID, 'password' => md5($password)])
            ->find();
        if ($res) {
            $this->success('解锁成功');
        }
        $this->error('解锁失败');
    }

    /**
     * 搜索框选中
     */
    public function search()
    {
        $id = input('id');
        $menu = Db::name('authRule')
            ->where('id', $id)
            ->find();
        if ($menu) {
            return json(['code' => '1', 'pid' => $menu['pid'], 'url' => url($menu['name'])]);
        }
        $this->error('查询失败');
    }

    /**
     * 介绍统计页
     */
    public function main()
    {
        $configInfo = [
            'version' => PHP_VERSION,
            'author' => 'zfy.',
            'home' => url('index/index/index', '', '', true),
            'server' => $_SERVER['SERVER_SOFTWARE'],
            'database' => db()->query('select version() version')[0]['version'],
            'maxupload' => get_cfg_var("upload_max_filesize") ? : "不允许上传附件",
            'maxtime' => get_cfg_var("max_execution_time") ? get_cfg_var("max_execution_time") . "秒" : "无限制",
        ];

        $this->assign('configInfo', $configInfo);
        return view();
    }

    /** 
     * 个人资料
     */
    public function user_info()
    {
        if ($this->request->isPost()) {
            $data = input('post.');
            $where = array('id' => session('id'), 'password' => md5($data['password']));

            if (!Db::name('admin_user')->where($where)->find()) {
                $this->error('旧密码错误');

            }
            if (empty($data['newPwd']) && $data['newPwd'] !== $data['confirmPwd']) {
                $this->error('新密码不一致或为空');
            }

            try {
                Db::name('admin_user')
                    ->where('id', session('id'))
                    ->update(['password' => md5($data['newPwd']), 'head_img' => $data['head_img']]);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            };

            $this->success('修改成功');
        }

        if ($this->request->isGet()) {
            $user = Db::name('admin_user')->where('id', session('id'))->find();
            $this->assign('user', $user);
            return view();
        }

    }


}
