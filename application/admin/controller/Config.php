<?php
namespace app\admin\controller;

use think\Db;

class Config extends Base
{
    //站点配置路径
    private $sitepath = './../config/site.php';
    //微信配置路径
    private $wechatpath = './../config/wechat.php';
    //七牛云配置路径
    private $qiniutpath = './../config/qiniu.php';

    /**
     * 站点设置
     */
    public function site()
    {
        $data = config('site.');
        $this->assign('data', $data);

        if ($this->request->isPost()) {
            $data = input('post.');
            $this->updateConfig($this->sitepath, $data);
        }
        return view();
    }

    /**
     * 微信配置
     */
    public function wechat()
    {
        $data = config('wechat.');
        $this->assign('data', $data);

        if ($this->request->isPost()) {
            $data = input('post.');
            $this->updateConfig($this->wechatpath, $data);
        }
        return view();
    }

    /**
     * 七牛云配置
     */
    public function qiniu()
    {
        $data = config('qiniu.');
        $this->assign('data', $data);

        if ($this->request->isPost()) {
            $data = input('post.');
            $this->updateConfig($this->qiniutpath, $data);
        }
        return view();
    }

    /**
     * 更新配置
     */
    public function updateConfig($path, $data)
    {
        $file = include $path;
        $config = array_merge($file, $data);
        //生成文件字符串
        $str = "<?php " . PHP_EOL . "return [" . PHP_EOL;
        foreach ($config as $key => $value) {
            $str .= '   ' . '\'' . $key . '\'' . '=>' . '\'' . $value . '\'' . ',' . PHP_EOL;
        }
        $str .= '];';
        //写入文件
        if (file_put_contents($path, $str)) {
            $this->success('更新成功');
        } else {
            $this->error('更新失败');
        }
    }





}
