<?php
namespace app\admin\controller;

use think\Db;

class Url extends Base
{
    /** 
     * 链接分类
     */
    public function type()
    {
        $data = Db::name('url_type')->order('sort', 'desc')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /** 
     * 链接分类添加修改
     */
    public function type_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'name' => input('name'),
                'sort' => input('sort'),
                'addtime' => date('Y-m-d H:i:s', time())
            ];

            //入库
            $db = 'url_type';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('url_type')->where('id', input('id'))->find();
            $this->assign('data', $data);
            return view();
        }
    }

    /** 
     * 链接列表
     */
    public function url()
    {
        $param = $this->request->param();
        $where = [];

        //时间
        if ($param['begin'] && $param['end']) {
            $begin = strtotime($param['begin']);
            $end = strtotime($param['end']);
            $where[] = ['u.addtime', 'between', [$begin, $end]];
        }
        //关键词
        if (!empty($param['name'])) {
            $where[] = ['u.name', '=', $param['name']];
        }
        //分类
        if ($param['type']) {
            $where[] = ['u.type', '=', $param['type']];
        }

        //搜索重新回第一页
        if ($this->request->isPost()) {
            $param['page'] = '1';
        }

        //列表
        $data = Db::name('url u')
            ->join('url_type ut', 'u.type=ut.id', 'left')
            ->where($where)
            ->field('u.*,ut.name as typename')
            ->order('u.sort', 'desc')->paginate(13, '', ['query' => $param, 'page' => $param['page']]);
        //分类
        $type = Db::name('url_type')->select();

        $this->assign([
            'data' => $data,
            'param' => $param,
            'type' => $type
        ]);

        return view();
    }

    /** 
     * 链接添加修改
     */
    public function url_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'url' => input('url'),
                'sort' => input('sort'),
                'name' => input('name'),
                'type' => input('type'),
                'addtime' => date('Y-m-d H:i:s', time())
            ];

            //入库
            $db = 'url';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('url')->where('id', input('id'))->find();
            $this->assign('data', $data);

            $type = Db::name('url_type')->order('sort', 'desc')->select();
            $this->assign('type', $type);
            return view();
        }
    }


}
