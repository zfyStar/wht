<?php
namespace app\admin\controller;

use think\Db;

class User extends Base
{

    /**
     * 用户列表
     */
    public function user()
    {
        $where = [];
   
        //时间
        if (input('begin') && input('end')) {
            $begin = strtotime(input('begin'));
            $end = strtotime(input('end'));
            $where['createtime'] = [['>', $begin], ['<', $end]];
        }
        //是否购买
        if (input('isbuyer') != 'all' && input('isbuyer') != null) {
            $where['isbuyer'] = input('isbuyer');
        }
        //关键词
        if (input('keywords')) {
            if (input('name') == 'id') {
                $where['id'] = input('keywords');
            } elseif (input('name') == 'realname') {
                $where['realname'] = input('keywords');
            }
        }
        //搜索重新回第一页
        if ($this->request->isPost()) {
            $this->request->get(['page' => 1]);
        }
        $data = Db::name('user')->where($where)->alias('u')
            ->join('__USER_GROUP__ g', 'u.user_group_id=g.id', 'LEFT')
            ->fieldRaw('u.*,g.name')->order('createtime desc')
            ->paginate(15, '', ['query' => $this->request->param()]);
        $this->assign('data', $data);
        return view();
    }

    /**
     * 用户修改
     */
    public function user_edit()
    {
        if ($this->request->isPost()) {

            $data = [
                'user_group_id' => input('user_group_id'),
                'money' => input('money'),
                'reference' => input('reference'),
                'isbuyer' => input('isbuyer')
            ];

            //入库
            $db = 'user';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('user')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }

        //用户组
        $group = Db::name('user_group')->field('id,name')->select();
        $this->assign('group', $group);
        return view();
    }



    /**
     * 用户组
     */
    public function user_group()
    {
        $data = Db::name('user_group')->paginate(13, '', ['type' => 'Layui', ]);
        $this->assign('data', $data);
        return view();
    }


    /**
     * 用户组添加修改
     */
    public function group_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'name' => input('name'),
                'level1rate' => input('level1rate'),
                'level2rate' => input('level2rate'),
                'bonusamount' => input('bonusamount'),
                'bonusrate' => input('bonusrate')
            ];

            //入库
            $db = 'user_group';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('user_group')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }

        return view();
    }





}
