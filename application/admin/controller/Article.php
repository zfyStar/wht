<?php
namespace app\admin\controller;

use com\Qiniu;
use com\Articles;
use think\Db;

class Article extends Base
{
    /**
     * 文章类型列表
     */
    public function category()
    {
        $data = Db::name('article_category')->order('addtime')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 文章分类添加修改
     */
    public function category_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'category_name' => input('category_name'),
                'sort' => input('sort'),
                'status' => input('status'),
                'addtime' => date('Y-m-d H:i:s', time())
            ];

            //入库
            $db = 'article_category';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('article_category')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }
        return view();
    }

    /**
     * 文章标签列表
     */
    public function tags()
    {
        $data = Db::name('tags')->order('addtime')->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 文章标签添加修改
     */
    public function tags_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'tags_name' => input('tags_name'),
                'sort' => input('sort'),
                'status' => input('status'),
                'addtime' => date('Y-m-d H:i:s', time())
            ];

            //入库
            $db = 'tags';
            $this->toDb($db, $data);
        }

        if ($this->request->isGet()) {
            $data = Db::name('tags')->where('id', input('id'))->find();
            $this->assign('data', $data);
        }
        return view();
    }

    /**
     * 文章列表
     */
    public function article()
    {
        $data = Db::name('article a')
            ->join('article_category c', 'a.category_id=c.id', 'left')
            ->field('a.*,c.category_name')
            ->paginate();
        $this->assign('data', $data);
        return view();
    }

    /**
     * 文章添加
     */
    public function article_add()
    {
        if ($this->request->isPost()) {

            $data = [
                'title' => input('title'),
                'cover' => input('cover'),
                'category_id' => input('category_id'),
                'content' => htmlspecialchars(input('content')),
                'addtime' => date('Y-m-d H:i:s', time())
            ];

            // 启动事务
            Db::startTrans();
            try {
                $articleId = Db::name('article')->insertGetId($data);

                //标签
                $tags = input('tags');
                foreach ($tags as $tid) {
                    Db::name('article_tags')->insert(['article_id' => $articleId, 'tags_id' => $tid]);
                }
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $this->error('添加失败');
            }
            $this->success('添加成功');
        }

        if ($this->request->isGet()) {
            //分类 
            $category = Db::name('article_category')->where('status', 1)->select();
            $this->assign('category', $category);
            //标签
            $tags = Db::name('tags')->where('status', 1)->select();
            $this->assign('tags', $tags);
            return view();
        }

    }

    /**
     * 文章修改
     */
    public function article_edit()
    {
        if ($this->request->isPost()) {
            $id = input('id');
            $data = [
                'title' => input('title'),
                'cover' => input('cover'),
                'category_id' => input('category_id'),
                'content' => htmlspecialchars(input('content')),
                'updtime' => date('Y-m-d H:i:s', time())
            ];

            // 启动事务
            Db::startTrans();
            try {
                Db::name('article')->where('id', $id)->update($data);

                //标签
                $tags = input('tags');
                Db::name('article_tags')->where('article_id', $id)->delete();
                foreach ($tags as $tid) {
                    Db::name('article_tags')->insert(['article_id' => $id, 'tags_id' => $tid]);
                }
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $this->error('修改失败');
            }
            $this->success('修改成功');
        }

        if ($this->request->isGet()) {
            $id = input('id');
            $data = Db::name('article')->where('id', $id)->find();
            $this->assign('data', $data);
            //分类 
            $category = Db::name('article_category')->where('status', 1)->select();
            $this->assign('category', $category);
            //标签
            $tags = Db::name('tags t')
                ->join('article_tags at', 't.id=at.tags_id and at.article_id= ' . $id, 'left')
                ->where('t.status', 1)
                ->field('t.*,at.article_id as aid')
                ->select();
            $this->assign('tags', $tags);

            return view();
        }

    }

    /**
     * 文章提取
     */
    public function article_url()
    {
        if ($this->request->isPost()) {
            $url = input('url', '', 'urldecode');
            $url = str_replace(' ', '', $url);

            $article = new Articles();
            $data = $article->getArticle($url);

            if ($data && !empty($data['title'])) {
                $data['type'] = input('type');
                $data['add_time'] = time();
                $rowId = Db::name('article')->insert($data);
                if ($rowId) {
                    $this->success('文章保存成功', url('article/article_add', array('id' => $rowId)));
                }
                $this->error('文章保存失败');
            }
            $this->error('文章获取失败');
        }

        $type = Db::name('article_type')->select();
        $this->assign('type', $type);
        return view();
    }

}
