//注意：选项卡 依赖 element 模块，否则无法进行功能性操作
//提交表单
layui.use(['form', 'jquery', 'element'], function () {
    var form = layui.form,
        $ = layui.jquery,
        element = layui.element;
    //监听提交
    form.on('submit(formDemo)', function (data) {
        var index = top.layer.msg('数据提交中，请稍候', { icon: 16, time: false, shade: 0.8 });
        var url = $(this).data('url'), post = $(this).parents('form').serialize();
        $.post(url, post, function (data) {
            top.layer.close(index);
            if (!data.msg) { return; }
            top.layer.msg(data.msg);
            //刷新页面
            window.location.reload();
        });
    });
});

