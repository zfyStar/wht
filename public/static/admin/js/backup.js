layui.use(['jquery'], function () {    
    var $ = layui.jquery;
    //备份表
    $('.backup').click(function (event) {
        var num = 0, url = $(this).data('url');
        var $checkbox = $('.links_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.links_list tbody input[type="checkbox"][name="checked"]:checked');
        //判断有没有勾选
        $.each($checkbox, function () {
            if ($(this).is(":checked")) { num = 1; }
        });

        //未选择
        if (num == 0) { top.layer.msg("请选择需要备份的数据表"); return; };

        top.layer.confirm('确定备份选中的数据表？', { icon: 3, title: '提示信息' }, function (index) {
            var index = top.layer.msg('备份中，请稍候', { icon: 16, time: false, shade: 0.8 });
            //生成表字符串
            var name = '';
            $.each($checked, function () {
                name += ',' + $(this).data('name');
            });
            //Ajax
            $.post(url, { name: name }, function (data) {
                layer.close(index);
                top.layer.msg(data.msg);
            });
        })
    });
    //删除备份表
    $('.b_del').click(function (event) {
        var time = $(this).data('time'), url = $(this).data('url'), _this = $(this);
        top.layer.confirm('确定删除这个备份文件？', { icon: 3, title: '提示信息' }, function (index) {
            var index = top.layer.msg('删除中，请稍候', { icon: 16, time: false, shade: 0.8 });
            //Ajax
            $.post(url, { time: time }, function (data) {
                layer.close(index);
                top.layer.msg(data.msg);
                if (data.code == '1') { _this.parents('tr').remove(); };
            });
        })
    });
    //导入还原
    $('.b_huan').click(function (event) {
        var time = $(this).data('time'), url = $(this).data('url'), _this = $(this);
        top.layer.confirm('确定还原这个备份文件？', { icon: 3, title: '提示信息' }, function (index) {
            var index = top.layer.msg('还原中，请稍候', { icon: 16, time: false, shade: 0.8 });
            //Ajax
            $.post(url, { time: time }, function (data) {
                layer.close(index);
                top.layer.msg(data.msg);
            });
        })
    });
    //单个优化表
    $('.b_you').click(function (event) {
        var name = $(this).data('name'), url = $(this).data('url');
        var index = top.layer.msg('优化中，请稍候', { icon: 16, time: false, shade: 0.8 });
        //Ajax
        $.post(url, { name: name, num: 1 }, function (data) {
            layer.close(index);
            top.layer.msg(data.msg);
        });
    });
    //多选优化
    $('#optimize').click(function (event) {
        var num = 0, url = $(this).data('url');
        var $checkbox = $('.links_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.links_list tbody input[type="checkbox"][name="checked"]:checked');
        //判断有没有勾选
        $.each($checkbox, function () {
            if ($(this).is(":checked")) { num = 1; }
        });

        //未选择
        if (num == 0) { top.layer.msg("请选择需要优化的数据表"); return; };

        top.layer.confirm('确定优化选中的数据表？', { icon: 3, title: '提示信息' }, function (index) {
            var index = top.layer.msg('优化中，请稍候', { icon: 16, time: false, shade: 0.8 });
            //生成表字符串
            var name = '';
            $.each($checked, function () {
                name += ',' + $(this).data('name');
            });
            //Ajax
            $.post(url, { name: name, num: 2 }, function (data) {
                layer.close(index);
                top.layer.msg(data.msg);
            });
        })
    });
    //单个修复表
    $('.b_xiu').click(function (event) {
        var name = $(this).data('name'), url = $(this).data('url');
        var index = top.layer.msg('修复中，请稍候', { icon: 16, time: false, shade: 0.8 });
        //Ajax
        $.post(url, { name: name, num: 1 }, function (data) {
            layer.close(index);
            top.layer.msg(data.msg);
        });
    });
    //多选修复
    $('#repair').click(function (event) {
        var num = 0, url = $(this).data('url');
        var $checkbox = $('.links_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.links_list tbody input[type="checkbox"][name="checked"]:checked');
        //判断有没有勾选
        $.each($checkbox, function () {
            if ($(this).is(":checked")) { num = 1; }
        });

        //未选择
        if (num == 0) { top.layer.msg("请选择需要修复的数据表"); return; };

        top.layer.confirm('确定修复选中的数据表？', { icon: 3, title: '提示信息' }, function (index) {
            var index = top.layer.msg('修复中，请稍候', { icon: 16, time: false, shade: 0.8 });
            //生成表字符串
            var name = '';
            $.each($checked, function () {
                name += ',' + $(this).data('name');
            });
            //Ajax
            $.post(url, { name: name, num: 2 }, function (data) {
                layer.close(index);
                top.layer.msg(data.msg);
            });
        })
    });

});