layui.use(['form', 'jquery'], function () {
    var form = layui.form,
        $ = layui.jquery;
    //-------------------------------------------列表页js----------------------------------------------------------------------------//
    //全选
    form.on('checkbox(allChoose)', function (data) {
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        child.each(function (index, item) {
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });
    //通过判断文章是否全部选中来确定全选按钮是否选中
    form.on("checkbox(choose)", function (data) {
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        var childChecked = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"]):checked')
        data.elem.checked;
        if (childChecked.length == child.length) {
            $(data.elem).parents('table').find('thead input#allChoose').get(0).checked = true;
        } else {
            $(data.elem).parents('table').find('thead input#allChoose').get(0).checked = false;
        }
        form.render('checkbox');
    })
    //更改状态
    form.on('switch(isShow)', function (data) {
        var index = layer.msg('修改中，请稍候', { icon: 16, time: false, shade: 0.8 });
        var _this = $(this), url = $(this).data('url');
        var post = {
            value: $(this).data('value'),
            date: $(this).data('date'),
            name: $(this).attr('name'),
        };
        var state = $(this).data('value') == 1 ? 0 : 1;
        $.post(url, post, function (data) {
            layer.close(index);
            layer.msg(data.msg);
            if (data.code == '1') {
                _this.data('value', state);
            };
        });
    })
    //编辑
    $("body").on("click", ".datas_edit", function () {
        var url = $(this).data('url');
        var index = layui.layer.open({
            title: "修改",
            type: 2,
            content: url,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function () {
            layui.layer.full(index);
        })
        layui.layer.full(index);
    })
    //删除
    $("body").on("click", ".datas_del", function () {
        var _this = $(this), id = $(this).data('id'), url = $(this).data('url'), db = $(this).data('db');
        layer.confirm('确定删除此信息？', { icon: 3, title: '提示信息' }, function (index) {
            $.post(url, { id: id, db: db }, function (data) {
                layer.msg(data.msg);
                if (data.code == '1') {
                    _this.parents('tr').remove();
                };
            });
        });
    })
    //添加
    $(".datas_add").click(function () {
        var content = $(this).data('url');
        var index = layui.layer.open({
            title: "添加",
            type: 2,
            content: content,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function () {
            layui.layer.full(index);
        })
        layui.layer.full(index);
    })
    //批量删除
    $(".all_del").click(function () {
        var _this = $(this), num = 0;
        var $checkbox = $('.links_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.links_list tbody input[type="checkbox"][name="checked"]:checked');
        //判断有没有勾选
        $.each($checkbox, function () {
            if ($(this).is(":checked")) {
                num = 1;
            }
        });
        if (num == 1) {
            top.layer.confirm('确定删除选中的信息？', { icon: 3, title: '提示信息' }, function (index) {
                var index = top.layer.msg('删除中，请稍候', { icon: 16, time: false, shade: 0.8 });
                //生成id字符串
                var id = '';
                $.each($checked, function () {
                    id += ',' + $(this).data('id');
                });
                //Ajax删除
                var url = _this.data('url'), db = _this.data('db');
                $.post(url, { id: id, db: db }, function (data) {
                    layer.close(index);
                    top.layer.msg(data.msg);
                    if (data.code == '1') {
                        $.each($checked, function () {
                            $(this).parents('tr').remove();
                        });
                    };
                });
            })
        } else {
            top.layer.msg("请选择需要删除的内容");
        }
    })
    //-------------------------------------------------提交页js----------------------------------------------------------------------------//
    //监听提交
    form.on('submit(formDemo)', function (data) {
        var index = top.layer.msg('数据提交中，请稍候', { icon: 16, time: false, shade: 0.8 });
        var url = $(this).data('url');
        $.post(url, $("#formid").serialize(), function (data) {
            top.layer.close(index);
            if (!data.msg) { return; }
            top.layer.msg(data.msg);
            layer.closeAll("iframe");
            //刷新父页面
            parent.location.reload();
        });
    });
});