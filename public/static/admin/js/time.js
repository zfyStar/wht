//时间选择
layui.use('laydate', function () {
    var laydate = layui.laydate;

    //开始日期
    var startDate = laydate.render({
        elem: '#begin',
        max: "2099-12-31",//设置一个默认最大值
        done: function (value, date) {
            endDate.config.min = {
                year: date.year,
                month: date.month - 1, //关键
                date: date.date,
                hours: 0,
                minutes: 0,
                seconds: 0
            };
        }
    });
    
    //结束日期
    var endDate = laydate.render({
        elem: '#end',//选择器结束时间
        min: "1970-1-1",//设置min默认最小值
        done: function (value, date) {
            startDate.config.max = {
                year: date.year,
                month: date.month - 1,//关键
                date: date.date,
                hours: 0,
                minutes: 0,
                seconds: 0
            }
        }
    });
   
});