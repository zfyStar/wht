//切换搜索栏
$('.search-tab span').click(function (e) {
    if ($(this).addClass("active").siblings(".active").removeClass("active"),
        $(".search-hidden").remove(),
        $(this).hasClass("baidu"))
        $(".search-form").attr("action", "https://www.baidu.com/s"),
            $(".search-keyword").attr({
                name: "word",
                placeholder: "百度一下，你就知道"
            })
    else if ($(this).hasClass("google"))
        $(".search-form").attr("action", "https://www.google.com/search"),
            $(".search-keyword").attr({
                name: "q",
                placeholder: "Google 搜索"
            })
    else if ($(this).hasClass("bing"))
        $(".search-form").attr("action", "https://cn.bing.com/search"),
            $(".search-keyword").attr({
                name: "q",
                placeholder: "微软 Bing 搜索"
            })
    else if ($(this).hasClass("segmentfault"))
        $(".search-form").attr("action", "https://segmentfault.com/search"),
            $(".search-keyword").attr({
                name: "q",
                placeholder: "SegmentFault 提问"
            })
    else if ($(this).hasClass("image")) {
        $(".search-form").attr("action", "https://cn.bing.com/images/search"),
            $(".search-keyword").attr({
                name: "q",
                placeholder: "海量图片搜索"
            })
        var i = new Image
        i.src = "https://images.google.com/favicon.ico?" + Date.now(),
            i.onload = function () {
                $(".search-form").attr("action", "https://www.google.com/search"),
                    $(".search-form").prepend('<input class="search-hidden" type="hidden" name="tbm" value="isch">')
            }
    } else if ($(this).hasClass("wiki")) {
        $(".search-form").attr("action", "https://baike.baidu.com/search"),
            $(".search-keyword").attr({
                name: "word",
                placeholder: "自由的百科全书"
            })
        var i = new Image
        i.src = "https://zh.wikipedia.org/favicon.ico?" + Date.now(),
            i.onload = function () {
                $(".search-form").attr("action", "https://zh.wikipedia.org/w/index.php"),
                    $(".search-keyword").attr("name", "search")
            }
    } else if ($(this).hasClass("torrent"))
        $(".search-form").attr("action", "https://zh.btdb.to/search"),
            $(".search-keyword").attr({
                name: "q",
                placeholder: "磁力链，种子搜索"
            })
    else if ($(this).hasClass("scholar")) {
        $(".search-form").attr("action", "https://xueshu.baidu.com/s"),
            $(".search-keyword").attr({
                name: "wd",
                placeholder: "中英文文献检索"
            })
        var i = new Image
        i.src = "https://scholar.google.com/favicon.ico?" + Date.now(),
            i.onload = function () {
                $(".search-form").attr("action", "https://scholar.google.com/scholar"),
                    $(".search-keyword").attr({
                        name: "q"
                    }),
                    $(".search-form").prepend('<input class="search-hidden" type="hidden" name="hl" value="zh-CN">')
            }
    }
    $(".search-keyword").focus()
});

//点击工具按钮
$("#setting-icon").on("click", function () {
    $('.work-link .info').hide();
    $("#tool").fadeIn(300);
})

//切换分类
$('.work-link .tab span').on('click', function () {
    var index = $(this).index();
    $(this).addClass("active").siblings(".active").removeClass("active");
    $('.work-link .info').hide().eq(index).fadeIn(300);
})

//链接点击
$('.work-link .info ul li a').on('click', function () {
    $.post('/index/jump', { id: $(this).data('id') }, function (res) {
        //无
    })
})

//切换背景
$("#setting-bkgd select").change(function () {
    $("body").css("background", $(this).val());
})

//添加链接
$("#url-add").on('click', function () {
    $.post('/index/add', $("#urlfrom").serialize(), function (data) {
        layer.msg(data.msg)
        if (data.code == 1) {
            $("#urlfrom input[type='text'] ").val('')
        }
    });
})

//保存设置
$("#setting-save").on('click', function () {
    $.post('/index/setting', $("#bgfrom").serialize(), function (data) {
        layer.msg(data.msg)
        if (data.code == 1) {
            window.location.reload();
        }
    });
})

//根据链接提取标题
$('#url-url').blur(function (e) {
    var url = $(this).val();
    if (url.length > 0) {
        $.post('/index/gettitle', { url: url }, function (data) {
            if (data.code == 1) {
                $('#url-name').val(data.msg);
            }
        });
    }
});