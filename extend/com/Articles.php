<?php
namespace com;

use com\Qiniu;

class Articles
{
	//允许的文章来源
    public $allow = array('mp.weixin.qq.com', 'm.toutiao.com', 'm.sohu.com', 'news.sina.cn', '3g.163.com');
    public $url = '';
    /**
     * 检查是否是可以抓取的
     */
    public function checkUrl($url)
    {
        $param = parse_url($url);
        if (in_array($param['host'], $this->allow)) {
            $source = explode('.', $param['host']);
            return $source[1] == '163' ? 'wangyi' : $source[1];
        } else {
            return false;
        }
    }

    /**
     * 获取内容
     */
    public function getArticle($url)
    {
        $source = $this->checkUrl($url);
        if ($source) {
            $this->url = $url;
            return $this->$source();
        } else {
            return false;
        }
    }
    /**
     * curl 请求
     */
    public function request($url)
    {
        $agent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1';
        $param = parse_url($url);
        $referer = $param['scheme'] . $param['host'];
        $result = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result['output'] = curl_exec($ch);
        $result['status'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $result;
    }

    /**
     * 获取图片后缀
     */
    public function getSuffix($img = '')
    {
        if ($img == '') return 'jpg';
        preg_match('/data-type=\"(\S{3,4})\"/i', $img, $match);
        if ($match) {
            $suffix = $match[1];
        } else {
            $suffix = end(explode('.', $img));
            if (strlen($suffix) > 4) $suffix = 'png';
        }
        return $suffix;
    }

    /**
     * 抓取远程图片
     */
    public function fectImages($content = array())
    {
        if (empty($content)) return false;
        preg_match_all('/<img.*?src=\"([^\"]*)\"/i', $content['content'], $images);
        if (isset($images[1]) && !empty($images[1])) {
            $lists = array_unique($images[1]);
            $source = array_unique($images[0]);
            $img = 'http://tupian.youxiangbang.cn' . '/';
            $qiniu = new Qiniu();
            foreach ($lists as $k => $v) {
                $newImg = 'fetch/' . date('Ymd') . '/' . uniqid() . '.' . $this->getSuffix($source[$k]);
                $qiniu->fetch($v, $newImg);
                $content['content'] = str_replace($v, $img . $newImg, $content['content']);
            }
            return $content;
        } else {
            return $content;
        }
    }

    /**
     * 微信公众号文章抓取
     */
    public function weixin()
    {
        $response = $this->request($this->url);
        if ($response['status'] == 200) {
            $output = $response['output'];
            preg_match('/<h2 class=\"rich_media_title\" id=\"activity-name\">([\s\S]*)<\/h2>/i', $output, $title);
            preg_match('/<div class=\"rich_media_content \" id=\"js_content\">([\s\S]*)<\/div>/iU', $output, $body);
			//内容图片需要转换一下
            $text = preg_replace('/data-src=\"/i', 'src="', $body[1]);
            if ($title[1] != '' && strpos($title[1], '/h2')) $title[1] = strip_tags($title[1]);
            $content = $this->fectImages(array('title' => trim(strip_tags($title[1])), 'content' => $text));
            return $content;
        } else {
            return false;
        }
    }

    /**
     * 今日头条文章抓取
     */
    public function toutiao()
    {
        $response = $this->request($this->url);
        if ($response['status'] == 200) {
            $output = $response['output'];
            preg_match('/<input type=\'hidden\' name=\'csrfmiddlewaretoken\' value=\'(.*)\' \/>/i', $output, $token);
            $data = json_decode(file_get_contents($this->url . 'info/?csrfmiddlewaretoken=' . $token[1]), true);
            $content = $this->fectImages(array('title' => $data['data']['title'], 'content' => $data['data']['content']));
            return $content;
        } else {
            return false;
        }
    }

    /**
     * 搜狐新闻抓取
     */
    public function sohu()
    {
        $response = $this->request($this->url);
        if ($response['status'] == 200) {
            $output = $response['output'];
            preg_match('/<h1 class="h1">(.*)<\/h1>/i', $output, $title);
            preg_match('/<p class="para">(.*)<\/p>/i', $output, $body);
            $content = $this->fectImages(array('title' => trim($title[1]), 'content' => $body[0]));
            return $content;
        } else {
            return false;
        }
    }

    /**
     * 新浪新闻抓取
     */
    public function sina()
    {
        $response = $this->request($this->url);
        if ($response['status'] == 200) {
            $output = $response['output'];
            preg_match('/<h1 class="art_title_h1">(.*)<\/h1>/i', $output, $title);
            preg_match_all('/<p class="art_t">(.*)<\/p>/i', $output, $body);
            $content = $this->fectImages(array('title' => trim($title[1]), 'content' => implode('', $body[0])));
            return $content;
        } else {
            return false;
        }
    }

    /**
     * 网易新闻抓取
     */
    public function wangyi()
    {
        preg_match('/docid=(.*)&/i', $this->url, $param);
        $docid = $param[1];
        $request = sprintf('http://3g.163.com/touch/article/%s/full.html', $docid);
        $response = $this->request($request);
        if ($response['status'] == 200) {
            $output = $response['output'];
            preg_match('/artiContent\((.*)\)/i', $output, $data);
            $result = json_decode($data[1], true);
            $content = $this->fectImages(array('title' => $result[$docid]['title'], 'content' => $result[$docid]['body']));
            return $content;
        } else {
            return false;
        }
    }
}