<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::domain('wht', 'index'); //wht绑定万花筒模块
Route::get('/', 'index/index'); //首页
Route::get('/page/:page', 'index/index'); //首页分页
Route::get('about', 'index/about'); //关于

Route::get('article/:id', 'index/article'); //文章页
Route::get('links', 'index/links'); //友联
Route::get('tags/:name/[:page]', 'index/tags'); //标签







